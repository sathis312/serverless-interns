To install serverless in local : npm install serverless -g
Export environment variables in the terminal : 
		export AWS_ACCESS_KEY_ID="..."
		export AWS_SECRET_ACCESS_KEY="..."
		export AWS_SESSION_TOKEN="..."
To run the project :
		sls offline
A new endpoint is created for the sample API.
	For example : POST | http://localhost:3000/dev/{{apiname}}                                         │
		